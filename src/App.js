import './App.css';
import React, { useState, useEffect } from 'react';
import axios from 'axios';
import Header from './components/ui/Header'
import WikiNav from "./components/ui/WikiNav.js";
import CharQuestion from "./components/ui/CharQuestion";

function App() {

    return (

        <div className='container'>
            <Header />
            <CharQuestion type='random'/>
            <WikiNav />
        </div>
    );
}
export default App;