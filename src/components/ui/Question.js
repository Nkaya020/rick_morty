import React from 'react';
// import React, { useState, useEffect } from 'react';
import Card from './Card'

class Question extends React.Component {

    constructor(props) {
        super(props);

        //this.state = this.initialState; //voor mooiere reset
        //this.initialState = {

        this.state = {
            question: props.question || 'Op de planeet x, kwam persoon a, persoon z tegen op locatie y?', //from episode x, location y with person z....
            correctAnswer: props.correctAnswer || 'ja', //of nee (true/false, 0/1), die moet ook op een bepaalde manier meegegeven worden
            points: props.points || 0,
            answeredRight: 'unknown',
            cardsAreVisible: true,
            explainbuttonIsVisible: true,
            nextbuttonIsVisible: true
        };
    }

    awnserYes = () => {
        if (this.props.correctAnswer === 'ja') {
            this.setState({ answeredRight: 'correct' });
            this.setState({ explainbuttonIsVisible: false });
        }

        else {
            this.setState({ answeredRight: 'false' });
            this.setState({ cardsAreVisible: false });
        }
        this.setState({ nextbuttonIsVisible: false });
    };

    awnserNo = () => {
        if (this.props.correctAnswer === 'nee') {
            this.setState({ answeredRight: 'correct' });
            this.setState({ explainbuttonIsVisible: false });
        }

        else {
            this.setState({ answeredRight: 'false' });
            this.setState({ cardsAreVisible: false });
        }
        this.setState({ nextbuttonIsVisible: false });
    };

    setCardsVisible = () => {
        this.setState({ cardsAreVisible: false });
    }

    reset = () => {
        //this.setState(this.initialState); kan dit niet mooier?
        this.setState({
            question: this.props.question || 'Op de planeet x, kwam persoon a, persoon z tegen op locatie y?', //from episode x, location y with person z....
            correctAnswer: this.props.correctAnswer || 'ja', //of nee (true/false, 0/1), die moet ook op een bepaalde manier meegegeven worden
            points: this.props.points || 0,
            answeredRight: 'unknown',
            cardsAreVisible: true,
            explainbuttonIsVisible: true,
            nextbuttonIsVisible: true
        })
    }

    render() {
        //console.log(this.props.char.name);
        console.log(this.props.loaded);
        return (
            <div>
                <div>
                    <h1 style={{ color: 'grey' }}>Question: {this.props.loaded ? this.props.char.name : 'loading'}</h1>
                    <h4 style={{ color: 'black' }}> {this.props.question} </h4>
                    <button onClick={this.awnserYes} style={{ color: 'green' }} >ja</button><button onClick={this.awnserNo} style={{ color: 'red' }}>nee</button>

                    <br></br><br></br><br></br>

                    {
                        this.state.explainbuttonIsVisible ? null :
                            <button onClick={this.setCardsVisible}>explain</button>
                    }

                    {
                        this.state.nextbuttonIsVisible ? null :
                            <button onClick={this.reset}>Next question</button>
                    }
                </div>

                {/* TODO: dit kan vast mooier */}
                <br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br>
                <br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br>
                <br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br>
                <br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br>

                {
                    this.state.cardsAreVisible ? null :
                        <section className='cards'>
                            {this.props.data.characters.map((character, index) =>
                                (<Card character={character} key={index} />
                                ))
                            }
                        </section>
                }

                {
                    this.state.cardsAreVisible ? null :
                        <section className='cards'>
                            {this.props.data.characters.map((character, index) =>
                                (<Card character={character} key={index} />
                                ))
                            }
                        </section>
                }
            </div>
        );
    }
}

export default Question