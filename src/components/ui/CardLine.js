import React from 'react';

class CardLine extends React.Component {

    render() {
        if(this.props.value == null || this.props.value == "") return null;

        return <li>{this.props.name ?? this.Capitalize(this.props.property)}: {this.props.value}</li>
    }

    Capitalize(str){
        return str.charAt(0).toUpperCase() + str.slice(1);
    }
}

export default CardLine;