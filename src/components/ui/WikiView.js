import React from 'react';
import Card from "./Card";

class WikiView extends React.Component {

    state = {
        data: null,
        loading: null,
    };

    render() {

        if (this.state.loading === true) {
            return <div>Loading..</div>
        }

        if (this.state.data === null && this.props.parentState.data === null) {
            return null;
        }

        const data = this.state.data ?? this.props.parentState.data;

        return <div>
            <div className={"pageList"}>
                {Array.from({length: data.info.pages}, (x, i) => i).map((i) => {
                    return <a href="#" className="page btn" onClick={() => this.props.loadCallback(this.props.baseUrl + "?page=" + (Number(i) + 1))}>{i + 1}</a>
                })}
            </div>
            <div className={"card-list"}>
                {data.results.map((d, i) => {
                    return <Card type={this.props.baseUrl.slice(32)} data={d} />
                })}
            </div>
        </div>
    }
}

export default WikiView;