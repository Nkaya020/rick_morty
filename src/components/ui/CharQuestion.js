import React from 'react';
// import React, { useState, useEffect } from 'react';
import Card from './Card'

class CharQuestion extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            loading: true,
            characters: null,
            question: '',
            correctAnswer: true,
            type: props.type || "status",
            answer: 'empty',
        }
    }

    async getCharacter() {
        const count = (await (await fetch("https://rickandmortyapi.com/api/character/"))
            .json()).info.count;
        const id = Math.floor(Math.random() * count);
        const url2 = "https://rickandmortyapi.com/api/character/" + id;
        return (await fetch(url2)).json();
    }

    async getCharacterFromURL(url) {
        const response = await fetch(url);
        return await response.json();
    }

    async getCharacterFromEpisode(episode, exclude) {
        const charURL = episode.characters[Math.floor(Math.random() * episode.characters.length)];
        //Check for double characters
        if (charURL === exclude) {
            return this.getCharacterFromEpisode(episode, exclude);
        }
        return await this.getCharacterFromURL(charURL);
    }

    async getEpisodeFromCharacter(character) {
        const epURL = character.episode[Math.floor(Math.random() * character.episode.length)];
        return await this.getEpisodeFromURL(epURL);
    }

    async getLocation() {
        const url = "https://rickandmortyapi.com/api/location/";
        const response = await fetch(url);
        const data = await response.json();
        const id = Math.floor(Math.random() * data.info.count);
        const url2 = "https://rickandmortyapi.com/api/location/" + id;
        const response2 = await fetch(url2);
        const data2 = await response2.json();
        return data2;
    }

    async getEpisode() {
        const url = "https://rickandmortyapi.com/api/episode/";
        const response = await fetch(url);
        const data = await response.json();
        const id = Math.floor(Math.random() * data.info.count);
        const url2 = "https://rickandmortyapi.com/api/episode/" + id;
        const response2 = await fetch(url2);
        const data2 = await response2.json();
        return data2;
    }

    async getEpisodeFromURL(url) {
        const response = await fetch(url);
        return await response.json();
    }

    async componentDidMount() {
        let characters = [];
        characters.push(await this.getCharacter());

        this.state.answer = null;

        let question = 'loading question...';
        let correctAnswer = true;

        const pickFrom = [
            'status',
            'origin',
            'location',
            'episode',
        ];

        this.setState({type: pickFrom[Math.floor(Math.random() * pickFrom.length)]});

        switch (this.state.type) {
            //Is [character] still alive?
            case 'status':
                question = 'Is ' + characters[0].name + ' still alive?';
                correctAnswer = (characters[0].status === 'Alive');
                break;
            //Does [character] come from [location]?
            case 'origin':
                correctAnswer = Math.random() < .5;

                let origin = null;
                if (!correctAnswer) {
                    origin = await this.getLocation();
                    origin = origin.name;
                    if (origin === characters[0].origin.name) correctAnswer = true;
                } else {
                    origin = characters[0].origin.name
                }

                question = 'Does ' + characters[0].name + ' come from ' + origin + '?'
                break;
            //Is [character] last seen at [location]?
            case 'location':
                correctAnswer = Math.random() < .5;

                let location = null;
                if (!correctAnswer) {
                    location = await this.getLocation();
                    location = location.name;
                    if (location === characters[0].location.name) correctAnswer = true;
                } else {
                    location = characters[0].location.name;
                }

                question = 'Is ' + characters[0].name + ' last seen at ' + location + '?';
                break;
            //Are [character1] and [character2] both in the episode: [episode]?
            case 'episode':
                let episode = await this.getEpisodeFromCharacter(characters[0]);
                correctAnswer = Math.random() < .5;

                if (correctAnswer) {
                    characters.push(await this.getCharacterFromEpisode(episode, characters[0].url));
                } else {
                    characters.push(await this.getCharacter());
                }

                question = 'Are ' + characters[0].name + ' and ' + characters[1].name + ' both in the episode: ' + episode.name + '?';
                break;
        }

        this.setState({
            characters: characters,
            loading: false,
            question: question,
            correctAnswer: correctAnswer,
        });
    }

    answer = (answer) => {
        if (answer === this.state.correctAnswer) {
            this.setState({answer: 'Correct!'});
        } else {
            this.setState({answer: 'Wrong!'});
        }
    }

    render() {
        if (this.state.loading || !this.state.characters) {
            return <div>loading...</div>
        }

        return <div className={"question"}>
            <div className={"speechBubble"}>
                <div className={"query black-text"}>
                    {this.state.question}
                </div>
            </div>
            <div className={"buttons"}>
                <button className={"btn success"} onClick={() => this.answer(true)}>yes</button>
                <button className={"btn danger"} onClick={() => this.answer(false)}>no</button>
                {this.state.answer !== null ?
                (<button className={"btn info float-right"}
                         onClick={() => this.componentDidMount()}>{this.state.answer} Next question</button>)
                    : null}
            </div>
            <div className={"portrait"}>
                {this.state.characters.map((character, index) =>
                    (<Card data={character} key={index} hideData={this.state.answer === null}/>
                    ))
                }
            </div>
        </div>
    }
}

export default CharQuestion