import React from 'react'
import CardLine from './CardLine';

class Card extends React.Component {

    render() {
        const data = this.props.data;
        return  <div className={'card'}>
            <div className={'card-inner'}>
                <div className={'card-front'}>
                    <img src={this.props.data.image ?? "https://rickandmortyapi.com/api/character/avatar/19.jpeg"} alt={"img"}/>
                </div>
                <div className={'card-back'}>
                    <h1>{this.props.data.name}</h1>
                    {this.props.hideData ?? false ? "Answer the question to reveal this!" : <ul>
                        <CardLine property={"status"} value={data.status} />
                        <CardLine property={"species"} value={data.species} />
                        <CardLine property={"type"} value={data.type} />
                        <CardLine property={"dimension"} value={data.dimension}/>
                        <CardLine property={"gender"} value={data.gender} />
                        <CardLine property={"origin"} value={data.origin?.name} name={"Origin"} />
                        <CardLine property={"location"} value={data.location?.name} name={"Location"} />
                        <CardLine property={"air_date"} value={data.air_date} name={"Air date"} />
                    </ul>}
                </div>
            </div>
        </div>
    }
}

export default Card