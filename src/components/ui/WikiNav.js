import React from 'react';
import WikiView from "./WikiView";

class WikiNav extends React.Component {

    state = {
        loading: false,
        data: null,
        url: null,
    };

    render() {
        return <div className={"bottom-nav justify"}>
            <button className={"btn info"}
                    onClick={() => this.load("https://rickandmortyapi.com/api/character")}>Characters
            </button>
            <button className={"btn info"}
                    onClick={() => this.load("https://rickandmortyapi.com/api/location")}>Locations
            </button>
            <button className={"btn info"}
                    onClick={() => this.load("https://rickandmortyapi.com/api/episode")}>Episodes
            </button>
            <WikiView loadCallback={this.load} mode={this.state.character} baseUrl={this.state.url} parentState={this.state}/>
        </div>
    }

    load = async (str) => {
        console.log("Nav loading " + str);
        this.setState({loading: true, data: null, url: str});
        const request = await fetch(str);
        const json = await request.json();
        this.setState({loading: false, data: json});
    }
}

export default WikiNav;